[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.tudelft.nl%2Fwb1135%2Finteractive-notebooks.git/main)

# Interactive Notebooks

Here, we gather notebooks that are to be shared through Binder. This provides an interactive way to engage with the examples and simulations without having to download the notebooks. 

## Requirements

The required packages for the notebooks in this repository are found in `requirements.txt` and automatically included by binder.
